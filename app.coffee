
###
Module dependencies.
###
express = require "express"
# books = require "./routes/books"
http = require "http"
path = require "path"
expressValidator = require("./validator").expressValidator
# check = require("./validator").check
app = express()

{mongo, db} = require("./db.coffee")


# all environments
app.set "port", process.env.PORT or 3000
app.use express.logger("dev")
app.use express.bodyParser()
app.use expressValidator()
app.use express.methodOverride()
app.use app.router

config =
  pagination:
    limit:3

# only for development
util = require "util"
inspect = (obj) ->
  console.log 'inspect: ' + util.inspect(obj,true,4)



app.start = (cb) ->
  # connect to database
  db.open (collection) ->
    app.collection = collection
    # Start the app
    http.createServer(app).listen app.get("port"), ->
      console.log "Express server listening on port " + app.get("port")
      cb()


app.render = (err, req, res, books, code) ->
  if err
    console.warn err
    books = err
  statusCode = code or 200
  console.log statusCode
  res.setHeader 'Content-Type', 'application/json'

  # if collection contains pagination, render pagination
  if books? and books.pagination?
    pagination = books.pagination
    current = pagination.current
    if pagination.current > 0
      if pagination.current == '1'
        pagination.previous = "/books/"
      else
        pagination.previous = "/books/?page=#{current-1}"
    if current < pagination.pages-1
      pagination.next = "/books/?page=#{current+1}"

  res.send statusCode, JSON.stringify books
  res.end()

validate_book = (req, res) ->
  req.assert('ISBN', 'Please provide a valid ISBN').isValidISBN()
  req.assert('title', 'Please provide a valid title').notEmpty()
  req.assert('author', 'Please provide a valid author').notEmpty()
  # optional value
  if typeof req.body.image != 'undefined' 
    req.assert('image', 'Please provide a valid URL').isUrl()

  err = req.validationErrors()
  if err
    app.render err, req, res, {}, 400
  err


app.books =
  #* list all books, or query
  all: (req, res) ->
    query = {}
    if typeof req.query.isbn != 'undefined'
      query.ISBN = req.query.isbn

    pagination =
      limit: config.pagination.limit
    if typeof req.query.page != 'undefined' && parseInt(req.query.page) > 0
      pagination.skip = req.query.page*config.pagination.limit
    
    app.collection.find(query , pagination).toArray (err, results) ->
      if err
        console.error "Error accessing db"
        throw err

      app.collection.count (err,count) ->
        data =
          books: results
          pagination:
            count:count
            pages: Math.ceil(count/config.pagination.limit)
            current:parseInt(req.query.page) or 0
      
        app.render err, req, res, data

  #* show one book
  # show: (req, res) ->
  #   app.render err, req, res,

  #* create book
  create: (req, res) ->
    err = validate_book req, res
    unless err
      data = req.body or {}

      app.collection.insert data, (err, docs) ->
        if err
          console.warn err.message
        else
          console.log 'book created'
        app.render err, req, res, docs

  #* delete book
  delete: (req, res) ->
    app.collection.remove {_id: new mongo.ObjectID(req.params.id)}, (err) ->
      if err
        console.warn err.message
      else 
        console.log 'book deleted'
      app.render err, req, res

  #* delete book
  update: (req, res) ->
    err = validate_book req, res
    unless err
      data = req.body or {}
      app.collection.update {_id: new mongo.ObjectID(req.params.id)}, data, {safe:true}, (err, docs) ->
        if err
          console.warn err.message
        else 
          console.log 'book updated'
        app.render err, req, res, docs


app.get "/books", app.books.all
# app.get "/books/:id", books.show
app.post "/books", app.books.create
app.put "/books/:id", app.books.update
app.delete "/books/:id", app.books.delete

# development only
app.use express.errorHandler()  if "development" is app.get("env")

# Export as module to integrate in ```server.coffee``` and tests
exports.app = app