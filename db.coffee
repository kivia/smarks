collection_name = "books"

exports.mongo = mongo = require("mongodb")
Server = mongo.Server
Db = mongo.Db
BSON = mongo.BSONPure
server = new Server("localhost", 27017, auto_reconnect: true)
client = new Db("stylebooks", server, {safe:false})


exports.db = 
  open: (cb) ->
    client.open (err, db) ->
      if err
        console.log "Connection to 'stylebooks' database failed!"
      else
        console.log "Connected to 'stylebooks' database"
        client.collection collection_name,
          strict: true
        , (err, collection) ->
          if err
            console.log "'#{collection_name}' collection not found. Creating '#{collection_name}' collection"

            client.collection collection_name, (err, collection) ->
              collection.insert {},
                safe: true
              , (err, result) ->
                if err
                  console.log "Could not create collection #{collection_name}"
                else
                  console.log "'#{collection_name}' created. And connected to DB."
                  cb(collection)                
          else
            console.log "Connected to collection: #{collection_name}"
            cb(collection)
  
  close: ->
    client.close()

