exports.expressValidator = expressValidator = require 'express-validator'

exports.expressValidator.Validator.prototype.isValidISBN = () ->
  isbn = @str

  reg = /^[1-9]([0-9]+[- ]){3}[xX0-9]$/
  result = reg.exec isbn
  if result == null
    @error @msg
  @


  # following code is from: http://neilang.com/entries/how-to-check-if-an-isbn-is-valid-in-javascript/

  # isbn = isbn.replace(/[^\dX]/g, "")
  # return false  unless isbn.length is 10
  # chars = isbn.split("")
  # chars[9] = 10  if chars[9].toUpperCase() is "X"
  # sum = 0
  # i = 0

  # while i < chars.length
  #   sum += ((10 - i) * parseInt(chars[i]))
  #   i++
  

  # if (sum % 11) is 0
  #   @error @msg
  # @
