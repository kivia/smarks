##### Prequesits
* node v0.10.18
* mongod should be running


**caution:** this app will create and modify a mongodb stylebooks with a collection named books


##### Installing
	
Clone this projecet or unzip the file.
Use `npm install` to install required packages.
	Type cake to see following commands.


##### Cake commands

	cake start                # cleaning collection
	cake data:clean           # cleaning collection
	cake data:seed            # seeding books
	cake test                 # run tests


start server
./node_modules/.bin/coffee server.coffee
run test
./node_modules/.bin/mocha




#### Testing on command line

Seed some test data into your mongodb. It will create the database `stylebooks`and the collection `books`for you. MongoDb should be running.

	cake data:seed
	
Start the app
	
	cake start



###### Getting list of 

	
	curl -i -H "Accept: application/json" http://localhost:3000/books/

	HTTP/1.1 200 OK
	X-Powered-By: Express
	Content-Type: application/json
	Content-Length: 59
	Date: Sat, 21 Sep 2013 12:57:04 GMT
	Connection: keep-alive

	{"books":[{"ISBN":"900-199-059-1","title":"Another Book","author":"Arturs Friend","image":"http://friend.ce","_id":"523d98341572050000000002"},{"ISBN":"961-153-449-2","title":"Biography","author":"Arthur Biograph","image":"http://king.do","_id":"523d98341572050000000003"},{"ISBN":"962-425-059-0","title":"The Book","author":"Famous Arthur","image":"http://famouar.ex","_id":"523d98341572050000000004"}],"pagination":{"count":12,"pages":4,"current":0,"next":"/books/?page=1"}}
	

###### Trying to create a book with invalid ISBN:

    curl -i -H "Accept: application/json" -X POST -d "ISBN=060-153-449-2&title=titleads&author=author&image=http://example.com" http://localhost:3000/books/
    
    HTTP/1.1 400 Bad Request
	X-Powered-By: Express
	Content-Type: application/json
	Content-Length: 78
	Date: Sat, 21 Sep 2013 13:04:56 GMT
	Connection: keep-alive

	[{"param":"ISBN","msg":"Please provide a valid ISBN","value":"060-153-449-2"}]
	
	
###### Creating a book
	url -i -H ication/json" -X POST -d "ISBN=160-153-449-2&title=titleads&author=author&image=http://example.com" http://localhost:3000/books/
	HTTP/1.1 200 OK
	X-Powered-By: Express
	Content-Type: application/json
	Content-Length: 125
	Date: Sat, 21 Sep 2013 13:06:59 GMT
	Connection: keep-alive

	[{"ISBN":"160-153-449-2","title":"titleads","author":"author","image":"http://example.com","_id":"523d99f3b63f3e0000000001"}]
	
	
