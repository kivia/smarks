exports.should = require('should')
exports.app = app = require("../../app.coffee").app
exports.request = require('request')
exports.util = require('util')
exports.factories = require('../factories')
exports.inspect = (obj) ->
  console.log 'inspect: ' + exports.util.inspect(obj,true,4)

exports.TEST_HOST = TEST_HOST = 'localhost'
exports.TEST_PORT = TEST_PORT = 3000
exports.TEST_BASE_URL = "http://#{TEST_HOST}:#{TEST_PORT}"

