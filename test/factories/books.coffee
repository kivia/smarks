
exports.data = ->
  book1:
    ISBN: "960-425-059-0"
    title: "The Book"
    author: "Famous Arthur"
    image: "http://famouar.ex"
  book2:
    ISBN: "900-199-059-1"
    title: "Another Book"
    author: "Arturs Friend"
    image: "http://friend.ce"
  book3:
    ISBN: "961-153-449-2"
    title: "Biography"
    author: "Arthur Biograph"
    image: "http://king.do"
  book4:
    ISBN: "962-425-059-0"
    title: "The Book"
    author: "Famous Arthur"
    image: "http://famouar.ex"
  book5:
    ISBN: "903-199-059-1"
    title: "Another Book"
    author: "Arturs Friend"
    image: "http://friend.ce"
  book6:
    ISBN: "964-153-449-2"
    title: "Biography"
    author: "Arthur Biograph"
    image: "http://king.do"
  book7:
    ISBN: "965-425-059-0"
    title: "Thomas Thomas"
    author: "Thomas"
    image: "http://thomas.es"
  book8:
    ISBN: "906-199-059-1"
    title: "The white tiger"
    author: "Arvind Ardiga"
    image: "http://addiiga.in"
  book9:
    ISBN: "967-153-449-2"
    title: "The tiger"
    author: "Ita Mita"
    image: "http://itamita.it"
  book10:
    ISBN: "968-425-059-0"
    title: "The Book of something"
    author: "Someone Special"
    image: "http://special.com"
  book11:
    ISBN: "909-219-059-1"
    title: "Spoon and Spoon"
    author: "Ada Loveless"
    image: "http://ada.com"
  book12:
    ISBN: "960-200-449-2"
    title: "Untold secrets of the Queen"
    author: "King Of Book"
    image: "http://kingofbook.my"


exports.invalid = ->
  invalid_isbn:
    ISBN: "060-153-449-2"
    title: "The invalid isbn"
    author: "No Name"
    image: "http://example.com"
